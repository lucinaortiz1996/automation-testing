package excelOperations;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestLoginPage {

	@SuppressWarnings({ "resource", "deprecation" })
	@Test
	public void testSignIn() throws IOException {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\lucin\\Desktop\\ChromeDriver.exe\\ChromeDriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://formy-project.herokuapp.com/form");

		ExcelReading data = new ExcelReading();
		String arrayExcelData[] = new String[7];

		arrayExcelData = data.excelData();
		LoginPage login = new LoginPage(driver);
		login.fName().sendKeys(arrayExcelData[0]);
		login.lName().sendKeys(arrayExcelData[1]);
		login.tJob().sendKeys(arrayExcelData[2]);
		login.education().click();
		login.sex().click();
		login.menu().click();
		login.option().click();
		login.date().sendKeys(arrayExcelData[6] + Keys.RETURN);
		login.submit().click();
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[role='alert']")));
		assertEquals(driver.findElement(By.xpath("//*/body/div/h1[starts-with(text(),'Thanks')]")).getText(),
				"Thanks for submitting your form");
	}
}
