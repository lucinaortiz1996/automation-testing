package assignment6;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {
	protected WebDriver driver;

	public LoginPage(WebDriver driver) {
		this.driver = driver;
	}

	private By fName = By.id("first-name");
	private By lName = By.id("last-name");
	private By jTitle = By.xpath("//*/input[@id='job-title']");
	private By education = By.id("radio-button-1");
	private By sex = By.xpath("//*/input[@id='checkbox-2']");
	private By menu = By.cssSelector("select[id='select-menu']");
	private By years = By.cssSelector("option[value='3']");
	private By dateMenu = By.cssSelector("input[id='datepicker']");
	private By submit = By.xpath("//*/a[contains(text(),'Submit')]");
	private By message = By.xpath("//*/body/div/h1[starts-with(text(),'Thanks')]");

	public WebElement fName() {
		return driver.findElement(fName);
	}

	public WebElement lName() {
		return driver.findElement(lName);
	}

	public WebElement tJob() {
		return driver.findElement(jTitle);
	}

	public WebElement education() {
		return driver.findElement(education);
	}

	public WebElement sex() {
		return driver.findElement(sex);
	}

	public WebElement menu() {
		return driver.findElement(menu);
	}

	public WebElement option() {
		return driver.findElement(years);
	}

	public WebElement date() {
		return driver.findElement(dateMenu);
	}

	public WebElement submit() {
		return driver.findElement(submit);
	}

	public WebElement message() {
		return driver.findElement(message);
	}


}
