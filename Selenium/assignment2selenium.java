package selenium2;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
public class assignment2selenium {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\lucin\\Downloads\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://formy-project.herokuapp.com/form");
        driver.findElement(By.cssSelector("#first-name")).sendKeys("Lucina");
        driver.findElement(By.cssSelector("#last-name")).sendKeys("Ortiz");
        driver.findElement(By.cssSelector("#job-title")).sendKeys("It trainee");
        driver.findElement(By.cssSelector("#radio-button-2")).click();
        driver.findElement(By.cssSelector("#checkbox-2")).click();
        driver.findElement(By.cssSelector("option[value='1']")).click();
        driver.findElement(By.cssSelector("#datepicker")).click();
        driver.findElement(By.cssSelector(".today.day")).click();
        driver.findElement(By.cssSelector("a[role='button']")).click();
        driver.close();
    }
}
