package exercise4;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class login {
@Test
public void loginpage() {
	System.setProperty("webdriver.chrome.driver", "C:\\Users\\lucin\\Desktop\\chromedriver.exe");
	WebDriver driver = new ChromeDriver(); 	
	driver.manage().window().maximize();
	driver.get("https://formy-project.herokuapp.com/form");
	
	@SuppressWarnings("deprecation")
	WebDriverWait d = new WebDriverWait(driver,5);
	
	selenium4 formpage = new selenium4(driver);
	formpage.firstName().sendKeys("Lucina");
	formpage.lastName().sendKeys("Ortiz");
	formpage.job().sendKeys("It trainee");
	formpage.education().click();
	formpage.sex().click();
	formpage.selectyears().click();
	formpage.years().click();
	formpage.date().sendKeys("01/16/1998"+Keys.RETURN);
	formpage.submit().click();
	d.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[role='alert']")));
	assertEquals(driver.findElement(By.cssSelector("[align='center']")).getText(), "Thanks for submitting your form");
}
}
