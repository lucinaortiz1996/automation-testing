package exercise4;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class selenium4 {
	WebDriver driver;
	public selenium4(WebDriver driver) {
		this.driver = driver;
	}
	
	By firstName = By.cssSelector("input[placeholder*='first']");
	By lastName = By.cssSelector("input[placeholder*='last");
	By job = By.cssSelector("input[placeholder*='job']");
	By education = By.cssSelector("input[value*='button-2']");
	By sex = By.id("checkbox-2");
	By selectyears = By.id("select-menu");
	By years = By.xpath("//option[@value='1']");
	By date = By.id("datepicker");
	By submit = By.cssSelector("a[class*='btn-primary']");
	By message = By.cssSelector("div[role='alert']");
	
	public WebElement firstName() {
		return driver.findElement(firstName);
	}
	public WebElement lastName() {
		return driver.findElement(lastName);
	}
	public WebElement job() {
		return driver.findElement(job);
	}
	public WebElement education() {
		return driver.findElement(education);
	}
	public WebElement sex() {
		return driver.findElement(sex);
	}
	public WebElement selectyears() {
		return driver.findElement(years);
	}
	public WebElement years() {
		return driver.findElement(years);
	}
	public WebElement date() {
		return driver.findElement(date);
	}
	public WebElement submit() {
		return driver.findElement(submit);
	}
	public WebElement message() {
		return driver.findElement(message);
	}

}
