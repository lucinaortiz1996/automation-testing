package assignment6;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestLoginPage {

	@SuppressWarnings({ "resource", "deprecation" })
	@Test
	public void testSignIn() throws Exception {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\lucin\\Desktop\\ChromeDriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://formy-project.herokuapp.com/form");

		reading data = new reading();
		String arrayExcelData[] = new String[6];

		arrayExcelData = data.excelData();
		LoginPage login = new LoginPage(driver);
		login.fName().sendKeys(arrayExcelData[0]);
		login.lName().sendKeys(arrayExcelData[1]);
		login.tJob().sendKeys(arrayExcelData[2]);
		login.education().click();
		login.sex().click();
		screenShot("screen 1",driver);
		login.menu().click();
		login.option().click();
		screenShot("screen 2",driver);
		login.date().sendKeys(arrayExcelData[5] + Keys.RETURN);
		screenShot("screen 3",driver);
		login.submit().click();
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[role='alert']")));
		screenShot("screen 4",driver);
		assertEquals(driver.findElement(By.xpath("//*/body/div/h1[starts-with(text(),'Thanks')]")).getText(),
				"Thanks for submitting your form");
	}
	public void screenShot(String name, WebDriver driver) throws Exception {
	    File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	    FileUtils.copyFile(scrFile, new File(".\\screenshots\\"+name+".png"));
	}
	
}
