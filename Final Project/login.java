package demo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class login {
	protected WebDriver driver;
	private By username = By.id("user-name");
	private By password = By.id("password");
	private By button = By.id("login-button");
	private By cart = By.xpath("//*/a[@class=\"shopping_cart_link\"]");
	private By add1 = By.id("add-to-cart-sauce-labs-backpack");
	private By add2 = By.id("add-to-cart-sauce-labs-bike-light");
	private By continua = By.id("checkout");
	private By firstname = By.id("first-name");
	private By lastname = By.id("last-name");
	private By zip = By.id("postal-code");
	private By continua1 = By.id("continue");
	private By finish = By.id("finish");

	public login(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement userName() {
		return driver.findElement(username);
	}

	public WebElement passWord() {
		return driver.findElement(password);

	}

	public WebElement button() {
		return driver.findElement(button);
	}

	public WebElement cart() {
		return driver.findElement(cart);
	}

	public WebElement add1() {
		return driver.findElement(add1);
	}

	public WebElement add2() {
		return driver.findElement(add2);
	}

	public WebElement continua() {
		return driver.findElement(continua);
	}

	public WebElement firstname() {
		return driver.findElement(firstname);
	}

	public WebElement lastname() {
		return driver.findElement(lastname);
	}

	public WebElement zip() {
		return driver.findElement(zip);
	}

	public WebElement continua1() {
		return driver.findElement(continua1);
	}

	public WebElement finish() {
		return driver.findElement(finish);
	}

	public void takeScreenShot(String screenShotName, WebDriver driver) throws Exception {
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File(".\\screenshot\\" + screenShotName + ".png"));
	}

	@SuppressWarnings("resource")
	public String[] excelData() throws IOException {
		// Location of the excelFile
		String excelPath = ".\\data\\database.xlsx";

		FileInputStream inputStream = new FileInputStream(excelPath);
		XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
		XSSFSheet sheet = workbook.getSheet("Sheet1");
		int rows = sheet.getLastRowNum();
		int columns = sheet.getRow(1).getLastCellNum();
		String array[] = new String[2];
		for (int r = 1; r <= rows; r++) {
			XSSFRow row = sheet.getRow(r);
			for (int c = 0; c < columns; c++) {
				XSSFCell cell = row.getCell(c);
				System.out.println(cell.getStringCellValue());
				array[c] = cell.getStringCellValue();

			}
		}

		return array;
	}

}
