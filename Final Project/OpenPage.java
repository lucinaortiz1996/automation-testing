package demo;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class OpenPage {
	// Public means that anyone can see it. Void is the type of
	@Test
	public void loginpage() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\lucin\\Desktop\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.saucedemo.com/");

		login signin = new login(driver);
		Thread.sleep(1000);
		signin.userName().sendKeys("standard_user");
		Thread.sleep(1000);
		signin.passWord().sendKeys("secret_sauce");
		signin.button().click();
	}

}
