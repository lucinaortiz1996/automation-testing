package demo;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Testcase6 {
	@Test
	public void buyoneitem() throws Exception {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\lucin\\Desktop\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.saucedemo.com/");
		@SuppressWarnings("deprecation")
		WebDriverWait d = new WebDriverWait(driver, 5);
		login signin = new login(driver);
		signin.userName().sendKeys("standard_user");
		signin.passWord().sendKeys("secret_sauce");
		Thread.sleep(1000);
		signin.button().click();
		d.until(ExpectedConditions.visibilityOfElementLocated(By.id("add-to-cart-sauce-labs-backpack")));
		signin.add1().click();
		signin.add2().click();
		Thread.sleep(1000);
		signin.cart().click();
		signin.continua().click();
		Thread.sleep(1000);
		String[] array = new String[3];
		array = signin.excelData();
		signin.firstname().sendKeys(array[0]);
		signin.lastname().sendKeys(array[1]);
		signin.zip().sendKeys("66614");
		Thread.sleep(1000);
		signin.continua1().click();
		Thread.sleep(1000);
		signin.finish().click();
		signin.takeScreenShot("captura3", driver);
	}
}
