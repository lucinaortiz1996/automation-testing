package demo;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Testcase4 {
	@Test

	public void Cart() throws Exception {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\lucin\\Desktop\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.saucedemo.com/");
		@SuppressWarnings("deprecation")
		WebDriverWait d = new WebDriverWait(driver, 5);
		login signin = new login(driver);
		signin.userName().sendKeys("standard_user");
		signin.passWord().sendKeys("secret_sauce");
		signin.button().click();
		d.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*/a[@class=\"shopping_cart_link\"]")));
		signin.cart().click();
	signin.takeScreenShot("captura1", driver);
	}
}
